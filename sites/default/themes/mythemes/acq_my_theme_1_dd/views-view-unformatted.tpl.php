<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) {print ' class="' . $classes_array[$id] . '"';} ?>>
    <?php print $row; ?>
  </div>
  <?php if ($id == 0 && $variables['ad_contents'][0] != NULL): ?>
    <div class="ad_front">

      <?php isset($variables['ad_contents'][0]) ? print render($variables['ad_contents'][0]) : NULL; ?>
    </div>
  <?php endif; ?>
  <?php if ($id == 2 && $variables['ad_contents'][1] != NULL): ?>
    <div class="ad_front">
      <?php isset($variables['ad_contents'][1]) ? print render($variables['ad_contents'][1]) : NULL; ?>
    </div>
  <?php endif; ?>
  <?php if ($id == 4 && $variables['ad_contents'][2] != NULL): ?>
    <div class="ad_front">
      <?php isset($variables['ad_contents'][2]) ? print render($variables['ad_contents'][2]) : NULL; ?>
    </div>
  <?php endif; ?>
  <?php if ($id == 6 && $variables['ad_contents'][3] != NULL): ?>
    <div class="ad_front">
      <?php isset($variables['ad_contents'][3]) ? print render($variables['ad_contents'][3]) : NULL; ?>
    </div>
  <?php endif; ?>
  <?php if ($id == 8 && $variables['ad_contents'][4] != NULL): ?>
    <div class="ad_front">

      <?php isset($variables['ad_contents'][4]) ? print render($variables['ad_contents'][4]) : NULL; ?>
    </div>
  <?php endif; ?>
<?php endforeach; ?>